#![feature(libc)]
extern crate libc;
extern crate wayland;

use std::{ptr, mem};
use libc::{c_int, c_char, c_void};

use wayland::client::{wl_proxy, wl_interface, wl_surface, wl_seat, wl_output, wl_array};


// TODO: XDG needs to be compiled against the compositor
// Add support for other compositors (only mutter so far)

#[repr(C)]
pub struct xdg_shell;
impl Copy for xdg_shell {}
#[repr(C)]
pub struct xdg_surface;
impl Copy for xdg_surface {}

#[repr(C)]
pub struct xdg_popup;
impl Copy for xdg_popup {}

#[cfg(feature = "mutter")] #[link(name = "mutter")]
extern "C" {
	pub static xdg_shell_interface: wl_interface;
	pub static xdg_surface_interface: wl_interface;
	pub static xdg_popup_interface: wl_interface;
}


/**
 * xdg_shell_version - latest protocol version
 * @XDG_SHELL_VERSION_CURRENT: Always the latest version
 *
 * The 'current' member of this enum gives the version of the protocol.
 * Implementations can compare this to the version they implement using
 * static_assert to ensure the protocol and implementation versions match.
 */
#[repr(C)]
pub enum xdg_shell_version {
	XDG_SHELL_VERSION_CURRENT = 4,
    __temp_do_not_use__,
}
impl Copy for xdg_shell_version {}


#[repr(C)]
pub enum xdg_shell_error {
	XDG_SHELL_ERROR_ROLE = 0,
    __temp_do_not_use__,
}
impl Copy for xdg_shell_error {}

/**
 * xdg_shell - create desktop-style surfaces
 * @ping: check if the client is alive
 *
 * This interface is implemented by servers that provide desktop-style
 * user interfaces.
 *
 * It allows clients to associate a xdg_surface with a basic surface.
 */
#[repr(C)]
pub struct xdg_shell_listener {
	/**
	 * ping - check if the client is alive
	 * @serial: pass this to the callback
	 *
	 * The ping event asks the client if it's still alive. Pass the
	 * serial specified in the event back to the compositor by sending
	 * a "pong" request back with the specified serial.
	 *
	 * Compositors can use this to determine if the client is still
	 * alive. It's unspecified what will happen if the client doesn't
	 * respond to the ping request, or in what timeframe. Clients
	 * should try to respond in a reasonable amount of time.
	 */
	pub ping: Option<extern "C" fn(data: *mut c_void, shell: *mut xdg_shell, serial: u32) -> ()>,
}
impl Copy for xdg_shell_listener {}

pub unsafe fn xdg_shell_add_listener(shell: *mut xdg_shell, listener: *const xdg_shell_listener, data: *mut c_void) -> c_int {
	wayland::client::wl_proxy_add_listener(shell as *mut wl_proxy, mem::transmute(listener), data)
}

pub const XDG_SHELL_USE_UNSTABLE_VERSION: u32 = 0;
pub const XDG_SHELL_GET_XDG_SURFACE: u32 = 1;
pub const XDG_SHELL_GET_XDG_POPUP: u32 = 2;
pub const XDG_SHELL_PONG: u32 = 3;

pub unsafe fn xdg_shell_set_user_data(shell: *mut xdg_shell, user_data: *mut c_void) {
	wayland::client::wl_proxy_set_user_data(shell as *mut wl_proxy, user_data);
}

pub unsafe fn xdg_shell_get_user_data(shell: *mut xdg_shell) {
	wayland::client::wl_proxy_get_user_data(shell as *mut wl_proxy);
}

pub unsafe fn xdg_shell_destroy(shell: *mut xdg_shell) {
	wayland::client::wl_proxy_destroy(shell as *mut wl_proxy);
}

pub unsafe fn xdg_shell_use_unstable_version(shell: *mut xdg_shell, version: i32) {
	wayland::client::wl_proxy_marshal(shell as *mut wl_proxy, XDG_SHELL_USE_UNSTABLE_VERSION, version);
}

pub unsafe fn xdg_shell_get_xdg_surface(shell: *mut xdg_shell, surface: *mut wl_surface) -> *mut xdg_surface {
	wayland::client::wl_proxy_marshal_constructor(shell as *mut wl_proxy, XDG_SHELL_GET_XDG_SURFACE, &xdg_surface_interface, ptr::null_mut::<usize>(), surface) as *mut xdg_surface
}

pub unsafe fn xdg_shell_get_xdg_popup(shell: *mut xdg_shell, surface: *mut wl_surface, parent: *mut wl_surface, seat: *mut wl_seat, serial: u32, x: i32, y: i32, flags: u32) -> *mut xdg_popup {
	wayland::client::wl_proxy_marshal_constructor(shell as *mut wl_proxy, XDG_SHELL_GET_XDG_POPUP, &xdg_popup_interface, ptr::null_mut::<usize>(), surface, parent, seat, serial, x, y, flags) as *mut xdg_popup
}

pub unsafe fn xdg_shell_pong(shell: *mut xdg_shell, serial: u32) {
	wayland::client::wl_proxy_marshal(shell as *mut wl_proxy, XDG_SHELL_PONG, serial);
}


/**
 * xdg_surface_resize_edge - edge values for resizing
 * @XDG_SURFACE_RESIZE_EDGE_NONE: (none)
 * @XDG_SURFACE_RESIZE_EDGE_TOP: (none)
 * @XDG_SURFACE_RESIZE_EDGE_BOTTOM: (none)
 * @XDG_SURFACE_RESIZE_EDGE_LEFT: (none)
 * @XDG_SURFACE_RESIZE_EDGE_TOP_LEFT: (none)
 * @XDG_SURFACE_RESIZE_EDGE_BOTTOM_LEFT: (none)
 * @XDG_SURFACE_RESIZE_EDGE_RIGHT: (none)
 * @XDG_SURFACE_RESIZE_EDGE_TOP_RIGHT: (none)
 * @XDG_SURFACE_RESIZE_EDGE_BOTTOM_RIGHT: (none)
 *
 * These values are used to indicate which edge of a surface is being
 * dragged in a resize operation. The server may use this information to
 * adapt its behavior, e.g. choose an appropriate cursor image.
 */
#[repr(C)]
pub enum xdg_surface_resize_edge {
	XDG_SURFACE_RESIZE_EDGE_NONE = 0,
	XDG_SURFACE_RESIZE_EDGE_TOP = 1,
	XDG_SURFACE_RESIZE_EDGE_BOTTOM = 2,
	XDG_SURFACE_RESIZE_EDGE_LEFT = 4,
	XDG_SURFACE_RESIZE_EDGE_TOP_LEFT = 5,
	XDG_SURFACE_RESIZE_EDGE_BOTTOM_LEFT = 6,
	XDG_SURFACE_RESIZE_EDGE_RIGHT = 8,
	XDG_SURFACE_RESIZE_EDGE_TOP_RIGHT = 9,
	XDG_SURFACE_RESIZE_EDGE_BOTTOM_RIGHT = 10,
}
impl Copy for xdg_surface_resize_edge {}


/**
 * xdg_surface_state - types of state on the surface
 * @XDG_SURFACE_STATE_MAXIMIZED: the surface is maximized
 * @XDG_SURFACE_STATE_FULLSCREEN: the surface is fullscreen
 * @XDG_SURFACE_STATE_RESIZING: (none)
 * @XDG_SURFACE_STATE_ACTIVATED: (none)
 *
 * The different state values used on the surface. This is designed for
 * state values like maximized, fullscreen. It is paired with the configure
 * event to ensure that both the client and the compositor setting the
 * state can be synchronized.
 *
 * States set in this way are double-buffered. They will get applied on the
 * next commit.
 *
 * Desktop environments may extend this enum by taking up a range of values
 * and documenting the range they chose in this description. They are not
 * required to document the values for the range that they chose. Ideally,
 * any good extensions from a desktop environment should make its way into
 * standardization into this enum.
 *
 * The current reserved ranges are:
 *
 * 0x0000 - 0x0FFF: xdg-shell core values, documented below. 0x1000 -
 * 0x1FFF: GNOME
 */
#[repr(C)]
pub enum xdg_surface_state {
	XDG_SURFACE_STATE_MAXIMIZED = 1,
	XDG_SURFACE_STATE_FULLSCREEN = 2,
	XDG_SURFACE_STATE_RESIZING = 3,
	XDG_SURFACE_STATE_ACTIVATED = 4,
}
impl Copy for xdg_surface_state {}


/**
 * xdg_surface - desktop-style metadata interface
 * @configure: suggest a surface change
 * @close: surface wants to be closed
 *
 * An interface that may be implemented by a wl_surface, for
 * implementations that provide a desktop-style user interface.
 *
 * It provides requests to treat surfaces like windows, allowing to set
 * properties like maximized, fullscreen, minimized, and to move and resize
 * them, and associate metadata like title and app id.
 *
 * On the server side the object is automatically destroyed when the
 * related wl_surface is destroyed. On client side, xdg_surface.destroy()
 * must be called before destroying the wl_surface object.
 */
#[repr(C)]
pub struct xdg_surface_listener {
	/**
	 * configure - suggest a surface change
	 * @width: (none)
	 * @height: (none)
	 * @states: (none)
	 * @serial: (none)
	 *
	 * The configure event asks the client to resize its surface.
	 *
	 * The width and height arguments specify a hint to the window
	 * about how its surface should be resized in window geometry
	 * coordinates. The states listed in the event specify how the
	 * width/height arguments should be interpreted.
	 *
	 * A client should arrange a new surface, and then send a
	 * ack_configure request with the serial sent in this configure
	 * event before attaching a new surface.
	 *
	 * If the client receives multiple configure events before it can
	 * respond to one, it is free to discard all but the last event it
	 * received.
	 */
	pub configure: Option<extern "C" fn (data: *mut c_void, surface: *mut xdg_surface, width: i32, height: i32, states: *mut wl_array, serial: u32) -> ()>,
	/**
	 * close - surface wants to be closed
	 *
	 * The close event is sent by the compositor when the user wants
	 * the surface to be closed. This should be equivalent to the user
	 * clicking the close button in client-side decorations, if your
	 * application has any...
	 *
	 * This is only a request that the user intends to close your
	 * window. The client may choose to ignore this request, or show a
	 * dialog to ask the user to save their data...
	 */
	pub close: Option<extern "C" fn (data: *mut c_void, surface: *mut xdg_surface) -> ()>,
}
impl Copy for xdg_surface_listener {}


pub unsafe fn xdg_surface_add_listener(surface: *mut xdg_surface, listener: *const xdg_surface_listener, data: *mut c_void) -> c_int {
	wayland::client::wl_proxy_add_listener(surface as *mut wl_proxy, mem::transmute(listener), data)
}

const XDG_SURFACE_DESTROY: u32 = 0;
const XDG_SURFACE_SET_PARENT: u32 = 1;
const XDG_SURFACE_SET_TITLE: u32 = 2;
const XDG_SURFACE_SET_APP_ID: u32 = 3;
const XDG_SURFACE_SHOW_WINDOW_MENU: u32 = 4;
const XDG_SURFACE_MOVE: u32 = 5;
const XDG_SURFACE_RESIZE: u32 = 6;
const XDG_SURFACE_ACK_CONFIGURE: u32 = 7;
const XDG_SURFACE_SET_WINDOW_GEOMETRY: u32 = 8;
const XDG_SURFACE_SET_MAXIMIZED: u32 = 9;
const XDG_SURFACE_UNSET_MAXIMIZED: u32 = 10;
const XDG_SURFACE_SET_FULLSCREEN: u32 = 11;
const XDG_SURFACE_UNSET_FULLSCREEN: u32 = 12;
const XDG_SURFACE_SET_MINIMIZED: u32 = 13;

pub unsafe fn xdg_surface_set_user_data(surface: *mut xdg_surface, user_data: *mut c_void) {
	wayland::client::wl_proxy_set_user_data(surface as *mut wl_proxy, user_data);
}

pub unsafe fn xdg_surface_get_user_data(surface: *mut xdg_surface) -> *mut c_void {
	wayland::client::wl_proxy_get_user_data(surface as *mut wl_proxy)
}

pub unsafe fn xdg_surface_destroy(surface: *mut xdg_surface) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_DESTROY);
	wayland::client::wl_proxy_destroy(surface as *mut wl_proxy);
}

pub unsafe fn xdg_surface_set_parent(surface: *mut xdg_surface, parent: *mut wl_surface) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_SET_PARENT, parent);
}

pub unsafe fn xdg_surface_set_title(surface: *mut xdg_surface, title: *const c_char) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_SET_TITLE, title);
}

pub unsafe fn xdg_surface_set_app_id(surface: *mut xdg_surface, app_id: *const c_char) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_SET_APP_ID, app_id);
}

pub unsafe fn xdg_surface_show_window_menu(surface: *mut xdg_surface, seat: *mut wl_seat, serial: u32, x: i32, y: i32) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_SHOW_WINDOW_MENU, seat, serial, x, y);
}

pub unsafe fn xdg_surface_move(surface: *mut xdg_surface, seat: *mut wl_seat, serial: u32) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_MOVE, seat, serial);
}

pub unsafe fn xdg_surface_resize(surface: *mut xdg_surface, seat: *mut wl_seat, serial: u32, edges: u32) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_RESIZE, seat, serial, edges);
}

pub unsafe fn xdg_surface_ack_configure(surface: *mut xdg_surface, serial: u32) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_ACK_CONFIGURE, serial);
}

pub unsafe fn xdg_surface_set_window_geometry(surface: *mut xdg_surface, x: i32, y: i32, width: i32, height: i32) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_SET_WINDOW_GEOMETRY, x, y, width, height);
}

pub unsafe fn xdg_surface_set_maximized(surface: *mut xdg_surface) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_SET_MAXIMIZED);
}

pub unsafe fn xdg_surface_unset_maximized(surface: *mut xdg_surface) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_UNSET_MAXIMIZED);
}

pub unsafe fn xdg_surface_set_fullscreen(surface: *mut xdg_surface, output: *mut wl_output) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_SET_FULLSCREEN, output);
}

pub unsafe fn xdg_surface_unset_fullscreen(surface: *mut xdg_surface) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_UNSET_FULLSCREEN);
}

pub unsafe fn xdg_surface_set_minimized(surface: *mut xdg_surface) {
	wayland::client::wl_proxy_marshal(surface as *mut wl_proxy, XDG_SURFACE_SET_MINIMIZED);
}

/**
 * xdg_popup - desktop-style metadata interface
 * @popup_done: popup interaction is done
 *
 * An interface that may be implemented by a wl_surface, for
 * implementations that provide a desktop-style popups/menus. A popup
 * surface is a transient surface with an added pointer grab.
 *
 * An existing implicit grab will be changed to owner-events mode, and the
 * popup grab will continue after the implicit grab ends (i.e. releasing
 * the mouse button does not cause the popup to be unmapped).
 *
 * The popup grab continues until the window is destroyed or a mouse button
 * is pressed in any other clients window. A click in any of the clients
 * surfaces is reported as normal, however, clicks in other clients
 * surfaces will be discarded and trigger the callback.
 *
 * The x and y arguments specify the locations of the upper left corner of
 * the surface relative to the upper left corner of the parent surface, in
 * surface local coordinates.
 *
 * xdg_popup surfaces are always transient for another surface.
 */
#[repr(C)]
pub struct xdg_popup_listener {
	/**
	 * popup_done - popup interaction is done
	 * @serial: serial of the implicit grab on the pointer
	 *
	 * The popup_done event is sent out when a popup grab is broken,
	 * that is, when the users clicks a surface that doesn't belong to
	 * the client owning the popup surface.
	 */
	pub popup_done: Option<extern "C" fn (data: *mut c_void, popup: *mut xdg_popup, serial: u32) -> ()>,
}
impl Copy for xdg_popup_listener {}

pub unsafe fn xdg_popup_add_listener(popup: *mut xdg_popup, listener: *const xdg_popup_listener, data: *mut c_void) -> c_int {
	wayland::client::wl_proxy_add_listener(popup as *mut wl_proxy, mem::transmute(listener), data)
}

pub const XDG_POPUP_DESTROY: u32 = 0;

pub unsafe fn xdg_popup_set_user_data(popup: *mut xdg_popup, user_data: *mut c_void) {
	wayland::client::wl_proxy_set_user_data(popup as *mut wl_proxy, user_data);
}

pub unsafe fn xdg_popup_get_user_data(popup: *mut xdg_popup) -> *mut c_void {
	wayland::client::wl_proxy_get_user_data(popup as *mut wl_proxy)
}

pub unsafe fn xdg_popup_destroy(popup: *mut xdg_popup) {
	wayland::client::wl_proxy_marshal(popup as *mut wl_proxy, XDG_POPUP_DESTROY);
	wayland::client::wl_proxy_destroy(popup as *mut wl_proxy);
}
